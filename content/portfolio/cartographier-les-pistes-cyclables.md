---
title: "Cartographier les pistes cyclables"
type: portfolio
date: 2018-12-06T12:51:00
submitDate: 11 Novembre 2018
caption: Aide citoyenne
image: images/portfolio/heatmap_banner.png
category: ["action","aide"]
client: Brice Frabre, Denis Feurer, Bruno Adelé
projectname: Dicussion sur Framasoft
projectlink: https://framavox.org/d/5dQzN3yV/cartographier-les-pistes-cyclables-sur-openstreetmap

---
### Cartographier les trajets quotidiens

Pourquoi cartographier vos trajets quotidiens ? Ceci permettra d'avoir des informations sur les zones les plus utilisées.

D'une part, cela permettra de savoir les zones les plus prioritaires et d'autre par cela permettra aux services de voierie de savoir quelles sont les zones à entretenir ou de sécuriser en priorité .

Pour cela il suffit d'un smartphone ou d'une montre connecté qui enregistre les points GPS lors de votre trajet. Il est préférable d'utiliser Strava, car celui-ci permet d'archiver l'ensemble de vos fichiers GPS dans un seul et unique fichier. L'équipe **#JeSuisUnDesDeux** développe ou contribue à concevoir des outils pour traiter l'ensemble des données GPS, notamment :

- https://gitlab.com/JeSuisUnDesDeux/strava-to-osm (Permet d'envoyer vos fichiers [Strava](https://strava.com) vers le site [OpenStreetMap](www.openstreetmap.org))
- https://gitlab.com/JeSuisUnDesDeux/download-gpx-from-osm  (Permet de télécharger depuis [OpenStreetMap](www.openstreetmap.org) l'ensemble des fichiers GPS avec le tag **JeSuisUnDesDeux**)
- https://github.com/jesuisundesdeux/strava-local-heatmap (Met en évidence les zones les plus utilisées)

#### Procédure pour envoyer les traces GPX Strava vers OSM
**Note:** L'utilisation de [Strava](https://strava.com) permet de faciliter le traitement des fichiers GPX. Sachez qu'il est possible d'exporter vos anciens fichier GPX par paire de 25

 1. Upload des GPX vers Strava ou envoie des traces via l'application mobile Strava ou montre connecté
 2. Téléchargement et décompression de l'archive Strava sur votre poste de travail
 3. Utilisation du scrip https://gitlab.com/JeSuisUnDesDeux/strava-to-osm pour uploader l'ensemble des fichiers vers OpenSteetMap

#### Exemple de zones les plus utilisées

**Version statique**

![heatmap](/images/portfolio/heatmap_cycle.png)

**Version dynamique**

[Version dynamique](https://umap.openstreetmap.fr/en/map/test_261644#15/43.6075/3.8900)

![umap](/images/portfolio/heatmap_umap.png)








### Cartographier les pistes cyclables

L'idée est de cartographier les pistes cyclables entre midi et 2 ou le week-end et de finir par un pique nique :)

Le but est de :
- Recenser l'état actuel des pistes cyclabes et actualiser l'état sur [OpenStreetMap](https://wiki.openstreetmap.org/wiki/FR:Bicycle), par la même occasion identifier les problèmes :

- linéaires (voie cyclable sur trottoir qui prend la place des piétons, voie avec un arbre tous les mètres)
- ponctuels permanents (sortie de voie cyclable hasardeuse, problème sur la voie)
- ponctuels "temporaires" (#GCUM et autres mauvais usages des voies cyclables). Sur ce sujet il faut être raccord avec tout ce qui existe dont http://www.velocite-montpellier.fr/velobs/

Pour cartographier toutes ces informations, il y a déjà des éléments dans la structure de données OSM, comme par exemple la propriété "segregated" et autres sur les "cycleway". Il faudrait que nous consultions https://wiki.openstreetmap.org/wiki/Key:cycleway et la communauté OSM MTP pour avoir des recommandations de bonnes pratiques de carto de voies cyclables.