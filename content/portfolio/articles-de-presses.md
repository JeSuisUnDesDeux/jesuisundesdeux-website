---
title: "Articles de presse"
type: portfolio
date: 2018-12-01T12:51:00
submitDate: 25 Octobre 2018
caption: Dans la presse
image: images/portfolio/presse_plus_etroit.jpg
category: ["information"]
client: Organe de presse

---
### La presse en parle 
(+ [#JeSuisUnDesDeux en temps réel sur **Twitter**](https://twitter.com/hashtag/jesuisundesdeux?f=tweets))

#### Septembre 2019

  - [\[ça parle #JeSuisUnDesDeux dans\] **LeParisien**: Le vélo en plein essor : près de 2 Français sur 5 l’utilisent dans leur quotidien](http://www.leparisien.fr/societe/le-velo-en-plein-essor-pres-de-2-francais-sur-5-l-utilisent-dans-leur-quotidien-19-09-2019-8155863.php)
  - [**France 3**: Montpellier : une piste cyclable symbolique dessinée pour dénoncer le manque d'équipements... rapidement effacée](https://france3-regions.francetvinfo.fr/occitanie/herault/montpellier/montpellier-piste-cyclable-symbolique-dessinee-denoncer-manque-equipements-remporte-grand-succes-1723457.html)
  - [**ViaOccitanie.tv**: Montpellier : Opération coup de poing pour plus de pistes cyclables](https://viaoccitanie.tv/montpellier-operation-coup-de-poing-pour-plus-de-pistes-cyclables/)
  - [**Midilibre**: Montpellier : une fausse piste cyclable pour protester contre le manque d'aménagements](https://www.midilibre.fr/2019/09/17/montpellier-une-fausse-piste-cyclable-pour-protester-contre-le-manque-damenagements,8418324.php)
  - [**La Gazette**: Arceaux : la "fausse" piste cyclable déjà retirée](http://www.lagazettedemontpellier.fr/66323/arceaux-la-fausse-piste-cyclable-deja-retiree.html)
  - [**Midilibre**: Montpellier : Une piste cyclable tracée par des militants](https://www.midilibre.fr/2019/09/16/montpellier-une-piste-cyclable-tracee-par-des-militants,8417103.php)
  - [**La Gazette**: Vélo : des militants créent une piste cyclable aux Arceaux](http://www.lagazettedemontpellier.fr/66290/velo-des-militants-creent-une-piste-cyclable-aux-arceaux.html)
  - [**LeMouvement.info**: \[VIDEO\] Montpellier 5h30 du mat, action pour la mobilité et l’urgence climatique](https://lemouvement.info/2019/09/16/videomontpellier-5h30-du-mat-action-pour-la-mobilite-et-lurgence-climatique/)


#### Août 2019

  - [\[ça parle #JeSuisUnDesDeux dans\] **LeMonde**:« J’ai fait ma vélorution » : ces villes prêtes à oublier la voiture](https://www.lemonde.fr/planete/article/2019/08/07/ces-villes-qui-decouvrent-les-vertus-du-velo_5497226_3244.html)

#### Juillet 2019
  - [**Europe 1**: Les pistes cyclables sortent de terre à vitesse grand V](https://www.europe1.fr/societe/les-pistes-cyclables-se-multiplient-en-france-mais-pas-en-campagne-3908214)

#### Juin 2019
  - [**20minutes**: \[ça parle vélo dans\] Trottinettes, tramway jusqu'à la mer, bus Métronomes... Les dossiers chauds du transport à Montpellier](https://www.20minutes.fr/montpellier/2542395-20190618-trottinettes-tramway-jusqu-mer-bus-metronomes-dossiers-chauds-transport-montpellier)  
  - [**LeMouvement.info**:\[Communiqué de presse\] Vélo à Montpellier, 15 millions de promesses électorales](https://lemouvement.info/2019/06/18/communique-de-presse-velo-a-montpellier-15-millions-de-promesses-electorales/) <!-- 18 juin -->
  - [**Midilibre**:Montpellier : le plan vélo hérite des dix millions prévus pour le stade](https://www.midilibre.fr/2019/06/18/montpellier-le-plan-velo-herite-des-dix-millions-prevus-pour-le-stade,8262435.php)
  - [**La Gazette**:Montpellier : 10 M€ pour le "Plan vélo" plutôt que pour le stade](http://www.lagazettedemontpellier.fr/60418/montpellier-10-meur-pour-le-plan-velo-plutot-que-pour-le-stade.html) <!-- 17 juin -->
  - [**La Gazette**:Montpellier : une balade à vélo au lever du soleil](http://www.lagazettedemontpellier.fr/60321/montpellier-une-balade-a-velo-au-lever-du-soleil.html) <!-- 16 juin -->
  - [**Actu.fr**:SunRide à Montpellier : première balade urbaine en vélo apaisée aux aurores](https://actu.fr/occitanie/montpellier_34172/sunride-montpellier-premiere-balade-urbaine-velo-apaisee-aurores_25039310.html) <!-- 16 juin -->

#### Mai 2019

  - [**LeMouvement.info**: \[Communiqué de presse\]\[Vélo à Montpellier\] : l’équipe de Saurel en flagrant délit d’enfumage](https://lemouvement.info/2019/05/28/communique-de-presse-velo-a-montpellier-lequipe-de-saurel-en-flagrant-delit-denfumage/) <!-- 28 mai -->
  - [**Midilibre**: \[Rencontre avec David Gasc\] Montpellier : ce qui vous attend ce jeudi 23 mai](https://www.midilibre.fr/2019/05/22/montpellier-ce-qui-vous-attend-ce-jeudi-23-mai,8214839.php)
  - [**Wheelz.fr:** Congrès FUB 2019 au Mans, le vélo vient de changer d’ère](https://www.weelz.fr/fr/congres-fub-2019-le-mans-loi-mobilites-plan-velo-nouvelle-ere/) <!-- 14 mai -->
  - [**Actu.fr**:Photographies à Montpellier : les lauréats des Boutographies qui se visitent à vélo](https://actu.fr/occitanie/montpellier_34172/photographies-montpellier-laureats-boutographies-se-visitent-velo_23571061.html) <!-- 7 mai -->
  - [**La Gazette**:Montpellier : Spécial Vélo de 20 pages](http://www.lagazettedemontpellier.fr/56753/la-gazette-est-en-kiosque-91.html) <!-- 2 mai -->

#### Avril 2019
  - [**e-metropolitain:** Montpellier : Vigilo, l’application des cyclistes qui veut épauler les collectivités](https://e-metropolitain.fr/2019/04/19/montpellier-vigilo-lapplication-cyclistes-veut-epauler-collectivites/)
  - [**LeMouvement.info**:Vigilo, l’appli qui va faire bosser toutes les métropoles sur les mobilités actives](https://lemouvement.info/2019/04/17/vigilo-lappli-qui-va-faire-bosser-toutes-les-metropoles-sur-les-mobilites-actives/)
  - [**LeMouvement.info**:L’autre maire possible pour Montpellier : Bruno Adele](https://lemouvement.info/2019/04/11/lautre-maire-possible-pour-montpellier-bruno-adele/)
  - [**Midilibre**:Montpellier : le hashtag #jesuisundesdeux passe à trois après le commentaire de Patrick Sébastien](https://www.midilibre.fr/2019/04/15/montpellier-le-hashtag-jesuisundesdeux-passe-a-trois-apres-le-commentaire-de-patrick-sebastien,8132310.php)
  - [**La Gazette**:Montpellier : une carte participative pour recenser les problèmes à vélo](http://www.lagazettedemontpellier.fr/54861/montpellier-une-carte-participative-pour-recenser-les-problemes-a-velo.html)<!-- 4 avril-->
  - [**ReporTerre.net**:À Montpellier, le vélo revendique sa place](https://reporterre.net/A-Montpellier-le-velo-revendique-sa-place) <!-- 4 avril -->
  - [**Midilibre**:Montpellier : une carte participative qui recense les points noirs cyclables et les "GCUM"](https://www.midilibre.fr/2019/04/02/montpellier-une-carte-participative-qui-recence-les-points-noirs-cyclables-et-les-gcum,8105599.php)
  - [**La Gazette**:1er avril : une fausse piste cyclable entre Montpellier et Castelnau](http://www.lagazettedemontpellier.fr/54652/1er-avril-une-fausse-piste-cyclable-entre-montpellier-et-castelnau.html)
  - [**e-metropolitain**:Action de cyclistes à Castelnau-le-Lez : des poissons d’avril… bien réels](https://e-metropolitain.fr/2019/04/01/action-de-cyclistes-a-castelnau-lez-poissons-davril-bien-reels/)

#### Mars 2019
  - [\[ça parle vélo dans\] **20minutes**:Municipales 2020 à Montpellier: Centre-ville, transports, propreté… Quels sont les enjeux du scrutin?](https://www.20minutes.fr/municipales/2472107-20190320-municipales-2020-montpellier-centre-ville-transports-proprete-enjeux-scrutin)
  - [**LaCroix**:#Velotaf, #surmaroute : les travailleurs à vélo se mobilisent sur Twitter](https://www.la-croix.com/Culture/Art-de-vivre/Velotaf-surmaroute-travailleurs-velo-mobilisent-Twitter-2019-03-20-1201010111)
  - [**Midilibre**:Montpellier : Philippe Saurel provoque de nouveau l’incompréhension des cyclistes](https://www.midilibre.fr/2019/03/19/montpellier-philippe-saurel-provoque-de-nouveau-lincomprehension-des-cyclistes,8076528.php)
  - [**e-metropolitain**:Montpellier : l’histoire compliquée entre Philippe Saurel et les mouvements citoyens](https://e-metropolitain.fr/2019/03/18/montpellier-lhistoire-compliquee-entre-philippe-saurel-et-les-mouvements-citoyens/)
  - [**LeMouvement.info**:\[Communiqué de presse\] Montpellier : la seule ville de France qui veut restreindre l’usage des vélos](https://lemouvement.info/2019/03/17/communique-de-presse-montpellier-la-seule-ville-de-france-qui-veut-restreindre-lusage-des-velos/)
  - [**LeMouvement.info**:\[VIDEOS & ITW\] Marche pour le climat, Montpellier à la recherche d’une solution vélo](https://lemouvement.info/2019/03/17/videos-itw-marche-pour-le-climat-montpellier-a-la-recherche-dune-solution-velo/)
  - [\[ça parle vélo dans\] **France Bleu**:VIDÉOS - Plus de 8.000 manifestants à Montpellier pour la Marche du Siècle pour le climat](https://www.francebleu.fr/infos/climat-environnement/photos-et-videos-plus-de-8000-manifestants-a-montpellier-pour-la-marche-du-siecle-pour-le-climat-1552755871)
  - [\[ça parle vélo dans\] **e-metropolitain**:Montpellier : un samedi vert et jaune](https://e-metropolitain.fr/2019/03/16/montpellier-samedi-vert-jaune/)
  - [\[ça parle vélo dans\] **ReporTerre.net**:En direct des manifestations des Gilets jaunes et pour le climat](https://reporterre.net/En-direct-des-manifestations-des-Gilets-jaunes-et-pour-le-climat)
  - [**La Gazette**:Montpellier : de fausses pistes cyclables tracées sur l'avenue de Toulouse](http://www.lagazettedemontpellier.fr/53528/montpellier-de-fausses-pistes-cyclables-tracees-sur-lavenue-de-toulouse.html)
  - [**LeMouvement.info**:Climat et convergences à Montpellier : des pistes cyclables symboliques](https://e-metropolitain.fr/2019/03/16/climat-convergences-a-montpellier-pistes-cyclables-symboliques/)
  - [**ReporTerre.net**:« Les vélos roulent pour le climat », à Montpellier (Hérault)](https://reporterre.net/Les-velos-roulent-pour-le-climat)<!--16 mars-->
  - [**Midilibre**:La petite reine star de la fin de semaine](https://www.midilibre.fr/2019/03/14/la-petite-reine-star-de-la-fin-de-semaine,8066791.php)
  - [**LeMouvement.info**:5e Marche pour le climat à vélo comme à pied, ils bougent pour notre avenir](https://lemouvement.info/2019/03/12/5e-marche-pour-le-climat-a-velo-comme-a-pied-ils-bougent-pour-notre-avenir/)
  - [**Midilibre**:Montpellier : ils vont rouler pour le climat](https://www.midilibre.fr/2019/03/11/montpellier-ils-vont-rouler-pour-le-climat,8061602.php)
  - [**LeMouvement.info**:Montpellier : « Les Vélos roulent pour le climat » pour une #solutionvelo, samedi 16 mars](https://lemouvement.info/2019/03/09/montpellier-les-velos-roulent-pour-le-climat-pour-une-solutionvelo-samedi-16-mars/)
  - [**La Gazette**:Marche pour le climat : une "convergence cycliste" de Clapiers vers la Comédie](http://www.lagazettedemontpellier.fr/52775/marche-pour-le-climat-une-convergence-cycliste-de-clapiers-vers-la-comedie.html)<!--7 mars-->
  - [**La Gazette**:Montpellier : une nouvelle "convergence cycliste" pour le climat](http://www.lagazettedemontpellier.fr/52082/montpellier-une-nouvelle-convergence-cycliste-pour-le-climat.html)<!--4 mars-->


#### Février 2019
  - [**e-metropolitain**:Montpellier : cinquième marche avec une « convergence cycliste »](https://e-metropolitain.fr/2019/02/28/montpellier-cinquieme-marche-convergence-cycliste/)
  - [**Midilibre**:Plan vélo : selon le président de la Fub, "Montpellier a le potentiel"](https://www.midilibre.fr/2019/02/04/plan-velo-selon-le-president-de-la-fub-montpellier-a-le-potentiel,7992762.php)
  - [**Midilibre**:Plan vélo à Montpellier : l’association Vélocité veut y croire](https://www.midilibre.fr/2019/02/04/plan-velo-a-montpellier-lassociation-velocite-veut-y-croire,7992761.php)
  - [**e-metropolitain**:Transports : la métropole de Montpellier valide un « Plan vélo »](https://e-metropolitain.fr/2019/02/01/metropole-de-montpellier-valide-plan-velo/)


#### Janvier 2019

  - [**e-metropolitain**:Vélocité à Montpellier : premier débat national et rencontre avec Philippe Saurel](https://e-metropolitain.fr/2019/01/28/velocite-a-montpellier-premier-debat-national-rencontre-philippe-saurel/)
  - [**PauseVélo**:Episode 12 : #JeSuisUnDesDeux](https://www.pausevelo.com/episode-12-jesuisundesdeux/) <!--23 janvier-->
  - [**lagglorieuse**:Montpellier à bicyclette](http://www.lagglorieuse.info/article_montpellier-bicyclette.html) <!--23 janvier-->
  - [**La Gazette**:Montpellier : un "die-in" pour sensibiliser aux violences routières](http://www.lagazettedemontpellier.fr/48577/montpellier-un-die-in-pour-sensibiliser-aux-violences-routieres.html)
  - [**LeMouvement.info**:Montpellier « Die-in » pour une priorité : construire une ville apaisée](https://lemouvement.info/2019/01/13/montpellier-die-in-pour-une-priorite-construire-une-ville-apaisee/)
  - [**Midilibre**:Montpellier : spectaculaire "die in" cycliste devant l'Hôtel de région](https://www.midilibre.fr/2019/01/12/montpellier-spectaculaire-die-in-cycliste-devant-lhotel-de-region,7942923.php)
  - [**France Bleu Hérault**:Une opération "die in" à Montpellier pour sensibiliser aux violences routières](https://www.francebleu.fr/infos/transports/une-operation-die-a-montpellier-pour-sensibiliser-aux-violences-routieres-1547291333)
  - [**France 3**:Montpellier : opération “die in” contre les violences routières](https://france3-regions.francetvinfo.fr/occitanie/herault/montpellier/montpellier-operation-die-in-contre-violences-routieres-1606055.html)
  - [**e-metropolitain**:Montpellier : un die-in de cyclistes pour une ville apaisée](https://e-metropolitain.fr/2019/01/12/montpellier-die-in-de-cyclistes-ville-apaisee/)
  - [**Midilibre**:Montpellier : un “die-in” organisé samedi pour sensibiliser à la cause piétonne](https://www.midilibre.fr/2019/01/11/montpellier-un-die-in-organise-samedi-pour-sensibiliser-a-la-cause-pietonne,7941180.php)
  - [**Mediapart**:Montpellier : Avec #JeSuisUnDesDeux le vélo s’invite dans le débat des municipales](https://blogs.mediapart.fr/methylbro/blog/110119/montpellier-avec-jesuisundesdeux-le-velo-s-invite-dans-le-debat-des-municipales)
  - [**LeMouvement.info**:Montpellier ce samedi 12 janvier, les cyclistes et les piétons vont faire un « Die-In »](https://lemouvement.info/2019/01/10/montpellier-ce-samedi-12-janvier-les-cyclistes-et-les-pietons-vont-faire-un-die-in/)
  - [**lagglorieuse**:Plan vélo : un budget sur la béquille ?](http://www.lagglorieuse.info/article_plan-v-lo-un-budget-sur-la-b-quille.html)
  - [**LeMouvement.info**:Jeudi 10 janvier – 18h30 à la Carmagnole – La place du vélo à Montpellier avec Mr BMX et Vélocité](https://lemouvement.info/2019/01/08/communique-de-presse-jeudi-10-janvier-18h30-a-la-carmagnole-la-place-du-velo-a-montpellier-avec-mr-bmx-et-velocite/)
  - [**Vélocité Grand Montpellier**:Publication du nouveau Schéma Directeur des Mobilités Actives (SDMA)](https://www.velocite-montpellier.fr/publication-du-nouveau-schema-directeur-des-mobilites-actives-sdma/)
  - [**Midilibre**:Le maire de Montpellier Philippe Saurel : "Sur le vélo, j'ai réagi"](https://www.midilibre.fr/2019/01/07/montpellier-philippe-saurel-joue-cartes-sur-table,7080746.php)
  - [**Midilibre**:Montpellier : Philippe Saurel offensif face à ses anciens amis socialistes](https://www.midilibre.fr/2019/01/03/montpellier-philippe-saurel-offensif-face-a-ses-anciens-amis-socialistes,6631487.php)
  - [**Midilibre**:Les grands chantiers à Montpellier : tramway, Grand-Rue, futur stade et vélos…](https://www.midilibre.fr/2019/01/02/les-grands-chantiers-a-montpellier-tramway-grand-rue-futur-stade-et-velos,6355682.php)

#### Décembre 2018


  - [**e-metropolitain**:Conseil municipal de Montpellier : la majorité à l’attaque](https://e-metropolitain.fr/2018/12/20/conseil-municipal-de-montpellier-la-majorite-a-lattaque/)
  - [**Midilibre**:Montpellier : "Le plan vélo va dans le bon sens mais il faut accélérer"](https://www.midilibre.fr/2018/12/04/montpellier-le-plan-velo-va-dans-le-bon-sens-mais-il-faut-accelerer,4998426.php)

#### Novembre 2018

  - [**La Gazette**:Exclusif : le plan vélo XXL de Saurel](http://www.lagazettedemontpellier.fr/45900/exclusif-le-plan-velo-xxl-de-saurel.html)
  - [**Midilibre**:"Je suis un des deux", la réponse des cyclistes à Philippe Saurel](https://www.midilibre.fr/2018/11/10/montpellier-je-suis-un-des-deux-la-reponse-des-cyclistes-a-philippe-saurel,4830605.php)
  - [**Midilibre**:Ils n'étaient pas 2, mais 1 200 à manifester à vélo](https://www.midilibre.fr/2018/11/10/montpellier-ils-netaient-pas-2-mais-1200-a-manifester-a-velo,4830666.php)
  - [**Mediapart**: #JeSuisUnDesDeux, ou comment le mépris a uni les vélotafeurs de Montpellier](https://blogs.mediapart.fr/brice-favre/blog/281018/jesuisundesdeux-ou-comment-le-mepris-uni-les-vélotafeurs-de-montpellier)
  - [**France 3**: Les cyclistes #JeSuisUnDesDeux étaient plus d'un millier battre le pavé en réponse à Philippe Saurel](https://france3-regions.francetvinfo.fr/occitanie/herault/montpellier/montpellier-cyclistes-jesuisundesdeux-etaient-plus-millier-battre-pave-reponse-philippe-saurel-1572752.html)
  - [**e-metropolitain**: En images / Montpellier : #Jesuisundesdeux rassemble 1 200 cyclistes](https://e-metropolitain.fr/2018/11/10/en-images-montpellier-jesuisundesdeux-rassemble-1-200-cyclistes/)
  - [**viaoccitanie**: #jesuisundesdeux : Les cyclistes mobilisés pour dénoncer le mauvais aménagement de Montpellier](https://viaoccitanie.tv/jesuisundesdeux-les-cyclistes-mobilises-pour-denoncer-le-mauvais-amenagement-de-montpellier/)
  - [**LeMouvement.info**: Montpellier : manifestation cycliste #jesuisundesdeux, samedi 10 novembre](https://lemouvement.info/2018/11/08/montpellier-manifestation-cycliste-jesuisundesdeux-samedi-10-novembre/)
  - [**LeMouvement.info**: Montpellier, manif citoyenne 1200 cyclistes en colère](https://lemouvement.info/2018/11/11/montpellier-manif-citoyenne-1200-cyclistes-en-colere/)
  - [**lagglorieuse**: #JeSuisUndesDeux](http://www.lagglorieuse.info/article_jesuisundesdeux.html)
  - [**20minutes**: On risque sa vie à bicyclette», près de 1.500 cyclistes réclament plus de sécurité](https://www.20minutes.fr/montpellier/2369479-20181111-video-montpellier-risque-vie-bicyclette-pres-1500-cyclistes-reclament-plus-securite)
  - [**Montpellier-metropole**: Quand le hashtag #JeSuisUnDesDeux des adeptes du vélo devient viral](http://www.montpellier-metropole.com/montpellier-quand-le-hashtag-jesuisundesdeux-des-adeptes-du-velo-devient-viral/)
  - [**francebleu**: Plus de 1.000 cyclistes dans les rues de Montpellier pour défendre la place du vélo dans la ville](https://www.francebleu.fr/infos/societe/plus-de-1000-cyclistes-dans-les-rues-de-montpellier-pour-defendre-la-place-du-velo-dans-la-ville-1541857423)
  - [**francoisbaraize**: La politique d’élimination des cyclistes](http://francoisbaraize.fr/la-politique-delimination-des-cyclistes/)


#### Octobre 2018

 - [**Midilibre**: Quand le hashtag #JeSuisUnDesDeux des adeptes du vélo devient viral](https://www.midilibre.fr/2018/10/25/montpellier-quand-le-hashtag-jesuisundesdeux-des-adeptes-du-velo-devient-viral,4745201.php)